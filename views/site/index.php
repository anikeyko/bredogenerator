<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Бредогенератор';
?>
<div class="site-index">

    <div class="body-content">

        <h1>Сгенерировать бред</h1>
        <?= Html::beginForm(['/'], 'POST', ['id'=>'generateForm', 'class'=>'form-horizontal']) ?>

        <div class="form-group field-text-title required">
            <label class="control-label" for="text-title">Количество предложений:</label>
            <?= Html::textInput('count', $count, ['type'=>'number', 'class'=>'form-control', 'min'=>1, 'max'=>1000]) ?>
            <div class="help-block"></div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Сгенерировать', ['class' => 'btn btn-success']) ?>
        </div>

        <?= Html::endForm() ?>
    </div>

    <?php if ($output): ?>
        <p>
            <?= $output ?>
        </p>
    <?php endif; ?>
</div>
