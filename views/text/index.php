<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тексты';
?>

<div class="text-index">

    <p>
        <?= Html::a('Добавить', 'add-text', ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>'{items}{pager}',
        'columns' => [
            'title',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'text-center text-grid-buttons'],
            ],
        ],
    ]); ?>


</div>
