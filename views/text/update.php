<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Text */

$this->title = 'Редактировать текст';
?>
<div class="text-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
