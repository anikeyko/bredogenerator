<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%texts}}`.
 */
class m190802_051849_create_texts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%texts}}', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(255)->notNull(),
            'text'=>$this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%texts}}');
    }
}
