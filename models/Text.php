<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Integer;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "texts".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 */
class Text extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'texts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['title', 'text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
        ];
    }

    /**
     * Возвращаем $count предложений случайным образом сгенерированных из базы
     * @param integer $count
     * @return string
     */
    public static function getRandomText(int $count)
    {
        $output = [];

        $texts = Text::find()->orderBy(new Expression('rand()'))->limit(5)->all();

        $texts_set = [];

        // разбиваем по точкам
        foreach ($texts as $text) {
            $texts_set[$text->id] = explode('.', $text->text);
        }

        // наполняем результат
        while (count($output) < $count) {
            $text_item = $texts_set[array_rand($texts_set)];
            $output[] = $text_item[array_rand($text_item)];
        }

        return join('. ', $output);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            // очищаем текст от лишних пробелов.
            $arr = explode('.', $this->text);
            array_walk($arr, function(&$item) {
                $item = trim($item);
            });
            $arr = array_filter($arr);
            $this->text = join('. ', $arr);

            return true;
        } else {
            return false;
        }
    }
}
